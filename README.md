# Requirements to contribute this project

To contribute this project, You need to know about some technologies. Here we listed these technologies:

- HTML5 & CSS3
- Vue, JavaScript framework
- npm (Node Package Manager)
- SASS Language
- Webpack

# Webpack Configuration in "./webpack.config.js"
![Webpack Configuration](/assets/images/readme-image-webpack.png)

# Project Directory Structure

- The output directory for CSS files is /dist/stylesheet
- The output directory for JS files is /dist/javascript
- CSS and JavaScript Libraries are imported from /lib directory and spearated with formats in there.
- You can run the scripts in 'package.json" and watch CSS/JS from /dev directory and export them /dist directory.
- The directory '/node_modules' is also uploaded, So you've to sync your Node Package Manager with it.
