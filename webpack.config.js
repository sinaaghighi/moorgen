const path = require('path');

module.exports = {
    entry: {
        components: "/dev/javascript/components.js",
        index: "/dev/javascript/index.js"
    },
    output: {
        filename: "[name].js",
        path: __dirname + "/dist/javascript"
    }
}